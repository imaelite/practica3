<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\listView;

$this->title = $titulo;
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?= $titulo ?></h1>
        <p><?= $desc ?></p>
    </div>
    <div class="body-content">
        <div class="row">
            <?php
           // 'layout' => "{pager}\n{summary}\n{items}\n{pager}",
            echo GridView::widget([
                'dataProvider' => $datos,
                'columns' => [
                     'apellido',
                ]
            ]);
            
//               echo ListView::widget([
//                'dataProvider' => $dataprovider,
//                'itemView' => '_consulta13',
//            ]);
            ?>
        </div>
    </div>
</div>
