<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Trabajadores;
use app\models\Delegacion;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                        [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout() {
        return $this->render('about');
    }

    public function actionConsulta12() {
        $datos = Trabajadores::find()->distinct()->all();

        $titulo = 'Consulta 12';

        $d = new ActiveDataProvider([
            "query" => $datos,
        ]);
        $desc = 'Listar todos los trabajadores';

        return $this->render('consultas', [
                    'datos' => $d,
                    'desc' => $desc,
                    'titulo' => $titulo,
        ]);
    }

    public function actionView() {
        $datos = Trabajadores::find()->distinct()->all();

        $titulo = 'Consulta 13';

        $d = new ActiveDataProvider([
            "query" => $datos,
        ]);
        $desc = 'Vista con todos los trabajdores';

        return $this->render('consultas', [
                    'datos' => $d,
                    'desc' => $desc,
                    'titulo' => $titulo,
        ]);
    }

    public function actionConsulta() {
        return $this->render('consulta');
    }

    public function actionDelegacion() {
        return $this->render('delegacion');
    }

    public function actionTrabajadores() {
        return $this->render('trabajadores');
    }

    public function actionConsulta14() {
        $titulo = 'Consulta 14';



        /* Active query */
        $datos = Delegacion::find()->distinct();


        /* Active record         */

        $b = $datos->all();

        /* ActiveDataProvider */
        $d = new ActiveDataProvider([
            "query" => $datos,
        ]);

        $desc = 'Listar todos los trabajadores';

        return $this->render('consultas', [
                    'datos' => $d,
                    'desc' => $desc,
                    'titulo' => $titulo,
        ]);
    }

    public function actionConsulta17_a() {
        $titulo = 'Consulta 17_a';

        $datos = Delegacion::find()->where(['poblacion' => 'santander']);

        /* Active record         */
        $d = $datos->all();

        $desc = 'Listar todos las delegaciones cuya poblacion sea Santander';

        return $this->render('consultas',[
                    'datos' => $d,
                    'desc' => $desc,
                    'titulo' => $titulo,
        ]);
    }
    
      public function actionConsulta17_b() {
        $titulo = 'Consulta 17_b';

        $datos = Delegacion::find()->where(['id' => 1]);

        /* Active record         */
        $d = $datos->all();

        $desc = 'Listar los trabajadores de la delegacion 1';

        return $this->render('consultas',[
                    'datos' => $d,
                    'desc' => $desc,
                    'titulo' => $titulo,
        ]);
    }
      public function actionConsulta17_c() {
        $titulo = 'Consulta 17_c';

        $datos = Trabajadores::find()->orderBy('nombre');

        /* Active record         */
        $d = $datos->all();

        $desc = 'Listar los trabajadores ordenados por nombre';

        return $this->render('consultas',[
                    'datos' => $d,
                    'desc' => $desc,
                    'titulo' => $titulo,
        ]);
    }
    
        public function actionConsulta17_d() {
        $titulo = 'Consulta 17_d';

        $datos = Trabajadores::find()->where(['=','fechaNacimiento','null']);

        /* Active record         */
        $d = $datos->all();

        $desc = 'Listar los trabajadores que no conozco la fecha de nacimiento';

        return $this->render('consultas',[
                    'datos' => $d,
                    'desc' => $desc,
                    'titulo' => $titulo,
        ]);
    }

}
